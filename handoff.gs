// Trigger: Run every minute

function handoff() {
  var threads = GmailApp.search('label:scripts is:unread');
  for(var i = threads.length - 1; i >= 0; i--) { // Iterate backwards for chronological order
    var scriptName = threads[i].getFirstMessageSubject();
    var messages = threads[i].getMessages();
    var firstUnread = 0;
    for(var j = messages.length - 1; j >= 0; j--) { // Iterate backwards to find first unread message
      if(!messages[j].isUnread()) {
        firstUnread = j + 1;
      }
    }
    for(var j = firstUnread; j < messages.length; j++) {
      var postdata = {'script': scriptName, 'body': messages[j].getBody(), 'time': messages[j].getDate()};
      Logger.log(UrlFetchApp.fetch('http://abstract.cs.washington.edu/~sunjayc/txt_scripts/passon.php', {'method': 'post', 'payload': postdata}).getContentText());
      messages[j].markRead();
    }
  }
}

