<?php

function run_script($args, $time) {
  $time = strtotime($time);
  $curtime = time();
  $data = parse_args($args);
  if(!isset($data[1]))
    $data[1] = '30 minutes';
  $timeout = strtotime($data[1], $time);
  if($curtime < $timeout)
    $last_location = $data[0];
  else
    $last_location = 'Location not specified';

  $loc_data = array($timeout, $last_location);
  file_put_contents('last_location', implode("\n", $loc_data)) or die('error');
}

if(!$CALLED_FROM_PASSON) {
  $curtime = time();
  $loc_data = file('last_location', FILE_IGNORE_NEW_LINES);
  $timeout = isset($loc_data[0]) ? $loc_data[0] : 0;
  if(($curtime < $timeout) && isset($loc_data[1]))
    $last_location = $loc_data[1];
  else
    $last_location = 'Location not specified';
  echo "<pre>\n";
  echo "Current location: " . $last_location . "\n";
  if($timeout > $curtime)
    echo "Time remaining: " . seconds_to_duration($timeout - $curtime) . "\n";
  echo "</pre>";
}

function parse_args($args) {
  return explode(";", $args);
}

function seconds_to_duration($seconds) {
  $days = 0;
  $hours = 0;
  $minutes = 0;

  while($seconds >= 24 * 60 * 60) {
    $days++;
    $seconds -= 24 * 60 * 60;
  }
  while($seconds >= 60 * 60) {
    $hours++;
    $seconds -= 60 * 60;
  }
  while($seconds >= 60) {
    $minutes++;
    $seconds -= 60;
  }

  return $days . " days, " . $hours . " hours, " . $minutes . " minutes, " . $seconds . "seconds";
}

?>
