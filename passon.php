<?php

ini_set('display_errors', 1);
error_reporting(E_ALL|E_STRICT);

date_default_timezone_set('America/Los_Angeles');

$scriptname = strtolower($_POST['script']);
$args = array_map('trim', explode('<br />', $_POST['body'], -5)); // Don't want AT&T's trailing junk
$time_sent = $_POST['time'];

if(file_exists($scriptname . '.php'))
{
  $CALLED_FROM_PASSON = true;
  include($scriptname . '.php');
  run_script($args, $time_sent);
}
?>
